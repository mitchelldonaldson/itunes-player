# Itunes Player Demo

## Technology

- Vue.js
- Less
- Itunes affiliate API (https://affiliate.itunes.apple.com/resources/documentation/itunes-store-web-service-search-api/#legal)
- Axios package for making calls to the api (https://www.npmjs.com/package/axios)
- This project was created with the vue.js cli (https://cli.vuejs.org/)
- Prettier is implemented as part of the linting process. (https://github.com/prettier/eslint-plugin-prettier)

#### Improvements
This project has been setup in as much of a way as possible that would replicate a large/complex application. But because of the timeframes/scope the following things I would suggest to improve the architecture and ability to scale the feature set.

- A dedicated state management system (Vuex)
- Caching search results for albums/songs
- Browser testing/support
- More specific error handling with bigger coverage
- Improving Unit/Integration tests coverage
- Allow for different media sizes based on screen resolution and/or screen size

#### Supported Browsers
Chrome Version 68 on Windows 10 is the supported browser

#### Supported Features
- The current playing song will always be highlighted and show a small music note icon next to it
- On mobile when a song is selected it will autoplay
- On mobile if the player is not shown already, it will be render fixed to the bottom of the screen
- On desktop when a song is selected it will only play when the play button is clicked
- If a song is already playing, it will continue playing until another selected song's play button has been clicked
- The fast forward/rewind buttons do not work and are for visual purpose only

## Setup/Running App

#### Requirements
This application has been built on Windows 10 using node v10.8 and npm v6.3. Other systems such as OSX have not been tested.

#### Dependency installation
```
npm install
```

#### Compiles and hot-reloads for development
```
npm run serve
```

#### Running tests
```
npm run test
```

## Deployment

Compiles and minifies for production
```
npm run build
```

The `dist` folder contains the output files from running this(index.html, minified css, minified js)

This can be deployed to any static hosting service. E.g. AWS S3, Netlify
