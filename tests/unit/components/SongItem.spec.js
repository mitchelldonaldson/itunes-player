import { expect } from "chai";
import { shallowMount } from "@vue/test-utils";
import { SongItem } from "../../../src/components";

describe("SongItem.vue", () => {
  const song = {
    trackName: "Track 01",
    artworkUrl60: "http://linkexample.com",
    trackId: 1,
    collectionName: "An Artist"
  };

  it("renders song data passed from props", () => {
    const wrapper = shallowMount(SongItem, {
      propsData: { song }
    });

    expect(wrapper.find("img").attributes()).to.have.property(
      "src",
      song.artworkUrl60
    );

    expect(wrapper.find("img").attributes()).to.have.property(
      "alt",
      song.collectionName
    );

    expect(wrapper.find("strong.SongItem-Text").text()).to.equal(
      song.trackName
    );
  });
});
