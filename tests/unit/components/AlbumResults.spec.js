import { expect } from "chai";
import { shallowMount } from "@vue/test-utils";
import { AlbumResults } from "../../../src/components";

describe("AlbumResults.vue", () => {
  it("renders a list of songs from an album passed as props", () => {
    const results = [
      {
        trackName: "Track 01",
        trackId: "1"
      },
      {
        trackName: "Track 02",
        trackId: "2"
      }
    ];

    const wrapper = shallowMount(AlbumResults, {
      propsData: { albumResults: results }
    });

    expect(wrapper.findAll("li")).to.be.lengthOf(results.length);
  });
});
