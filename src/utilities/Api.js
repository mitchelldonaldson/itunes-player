import axios from "axios";

class Api {
  static endpoint(type) {
    const endpoints = {
      search: "https://itunes.apple.com/search?limit=25&term=",
      album: "https://itunes.apple.com/lookup?entity=song&id="
    };
    return endpoints[type];
  }

  static async get(term, type) {
    try {
      const { data } = await axios.get(`${this.endpoint(type)}${term}`);
      return data;
    } catch (error) {
      return error;
    }
  }
}

export default Api;
