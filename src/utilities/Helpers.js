const config = {
  desktop: 992
};

class Helpers {
  static isDesktop() {
    return window.innerWidth > config.desktop;
  }
}

export default Helpers;
