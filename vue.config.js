module.exports = {
  css: {
    loaderOptions: {
      less: {
        data: `@import "@/config/variables.less";`
      }
    }
  }
};
